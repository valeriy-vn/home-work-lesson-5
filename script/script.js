//  Обьект
const objectDocument = {
  // Свойства
  title: '',
  body: '',
  footer: '',
  data: '',

  // Методы
  renameField(key) {
    if (key === 'title') this.title = prompt('Enter title');
    if (key === 'body') this.body = prompt('Enter body');
    if (key === 'footer') this.footer = prompt('Enter footer');
    if (key === 'data') this.data = prompt('Enter data');

  },

  // Вложеный обьэкт
  application: {
    // Вложеные обьэкты
    title: {},
    body: {},
    footer: {},
    data: {},
  },
};

objectDocument.renameField((key = 'title'));
objectDocument.renameField((key = 'body'));
objectDocument.renameField((key = 'footer'));
objectDocument.renameField((key = 'data'));

console.log(objectDocument.title);
console.log(objectDocument.body);
console.log(objectDocument.footer);
console.log(objectDocument.data);
